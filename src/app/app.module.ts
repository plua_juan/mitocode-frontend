import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PacienteComponent} from './pages/paciente/paciente.component';
import {HttpClientModule} from "@angular/common/http";
import {MaterialModule} from "./material/material.module";
import {PacienteEdicionComponent} from './pages/paciente/paciente-edicion/paciente-edicion.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MedicoComponent} from './pages/medico/medico.component';
import {MedicoDialogoComponent} from './pages/medico/medico-dialogo/medico-dialogo.component';
import {ExamenComponent} from './pages/examen/examen.component';
import {ExamenEdicionComponent} from './pages/examen/examen-edicion/examen-edicion.component';
import {EspecialidadComponent} from './pages/especialidad/especialidad.component';
import {EspecialidadEdicionComponent} from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import {ConsultaComponent} from './pages/consulta/consulta.component';
import {EspecialComponent} from './pages/consulta/especial/especial.component';
import {SignoComponent} from './pages/signo/signo.component';
import {SignoEdicionComponent} from './pages/signo/signo-edicion/signo-edicion.component';
import {BuscarComponent} from './pages/buscar/buscar.component';
import {PacienteDialogoComponent} from './pages/paciente/paciente-dialogo/paciente-dialogo.component';
import {DialogoDetalleComponent} from './pages/buscar/dialogo-detalle/dialogo-detalle.component';
import {ReporteComponent} from './pages/reporte/reporte.component';
import {PdfViewerModule} from "ng2-pdf-viewer";
import {LoginComponent} from './login/login.component';
import {JwtModule} from "@auth0/angular-jwt";
import {environment} from "../environments/environment";
import {UsuarioComponent} from './pages/usuario/usuario.component';
import {PerfilComponent} from './pages/usuario/perfil/perfil.component';
import { Not403Component } from './pages/not403/not403.component';

export function tokenGetter() {
  let tk = sessionStorage.getItem(environment.TOKEN_NAME);
  let token = tk != null ? tk : '';
  //console.log(" app.module.ts -->" + token);
  return token;
}

@NgModule({
  declarations: [
    AppComponent,
    PacienteComponent,
    PacienteEdicionComponent,
    MedicoComponent,
    MedicoDialogoComponent,
    ExamenComponent,
    ExamenEdicionComponent,
    EspecialidadComponent,
    EspecialidadEdicionComponent,
    ConsultaComponent,
    EspecialComponent,
    SignoComponent,
    SignoEdicionComponent,
    BuscarComponent,
    PacienteDialogoComponent,
    DialogoDetalleComponent,
    ReporteComponent,
    LoginComponent,
    UsuarioComponent,
    PerfilComponent,
    Not403Component
  ],
  entryComponents: [MedicoDialogoComponent, PacienteDialogoComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    PdfViewerModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: [environment.DOMAINS],
        blacklistedRoutes: ['http://localhost:8080/login/enviarCorreo']
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

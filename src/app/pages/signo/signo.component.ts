import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {PacienteService} from "../../_service/paciente.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {SignoService} from "../../_service/signo.service";
import {Signo} from "../../_model/signo";
import {ActivatedRoute} from "@angular/router";
import {FiltroSignoDTO} from "../../_dto/filtroSignoDTO";

@Component({
  selector: 'app-signos',
  templateUrl: './signo.component.html',
  styleUrls: ['./signo.component.css']
})
export class SignoComponent implements OnInit {
  form: FormGroup;
  displayedColumns = ['id', 'paciente', 'fecha', 'temperatura', 'pulso', 'ritmo', 'acciones'];
  dataSource: MatTableDataSource<Signo>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  mensaje: string;

  constructor(private signoService: SignoService, private pacienteService: PacienteService, private snackBar: MatSnackBar, public route: ActivatedRoute) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      'dni': new FormControl(''),
      'nombreCompleto': new FormControl(''),
      'fechaConsulta': new FormControl()
    });

    this.signoService.signoCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.signoService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

    this.signoService.listar().subscribe(data => {
      console.log(data);
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }

  busqueda() {

    let filtro = new FiltroSignoDTO(this.form.value['dni'], this.form.value['nombreCompleto'], this.form.value['fechaConsulta']);
    filtro.nombreCompleto = filtro.nombreCompleto.toLowerCase();
    if (filtro.fechaConsulta) {
      delete filtro.dni;
      delete filtro.nombreCompleto;
      this.signoService.buscar(filtro).subscribe(data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    } else {

      delete filtro.fechaConsulta;
      if (filtro.dni.length === 0) {
        delete filtro.dni;
      }
      if (filtro.nombreCompleto.length === 0) {
        delete filtro.nombreCompleto
      }
      this.signoService.buscar(filtro).subscribe(data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    }
    this.form = new FormGroup({
       'nombreCompleto': new FormControl(''),
      'dni': new FormControl(''),
      'fechaConsulta': new FormControl('')
    });
  }

  eliminar(idSigno: number) {
    this.signoService.eliminar(idSigno).subscribe(() => {
      this.signoService.listar().subscribe(data => {
        this.signoService.signoCambio.next(data);
        this.signoService.mensajeCambio.next('SE ELIMINO');
      });
    });
  }

}

import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Paciente} from "../../../_model/paciente";
import {PacienteService} from "../../../_service/paciente.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";
import {MatDialog} from "@angular/material/dialog";
import {PacienteDialogoComponent} from "../../paciente/paciente-dialogo/paciente-dialogo.component";
import {Signo} from "../../../_model/signo";
import {SignoService} from "../../../_service/signo.service";
import {ActivatedRoute, Params, Router} from "@angular/router";

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  id: number;
  form: FormGroup;
  edicion: boolean = false;
  signo: Signo;

  pacientes: Paciente[] = [];
  myControlPaciente: FormControl = new FormControl();
  pacienteSeleccionado: Paciente;
  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  filteredOptionsPaciente: Observable<any[]>;

  constructor(private signoService: SignoService, private pacienteService: PacienteService, private snackBar: MatSnackBar, private dialog: MatDialog, private route: ActivatedRoute, private router: Router) {
    this.signo = new Signo();
    this.form = new FormGroup({
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl('')
    });

  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.listarPacientes();
      this.iniciaForm();
    });


  }

  iniciaForm() {
    this.filteredOptionsPaciente = this.myControlPaciente.valueChanges.pipe(map(val => this.filter(val)));
    if (this.edicion) {
      this.signoService.listarPorId(this.id).subscribe(data => {
        let paciente = data.paciente;
        console.log(paciente);
        this.pacienteSeleccionado = paciente;
        this.form = new FormGroup({
          'paciente': this.myControlPaciente,
          'id': new FormControl(data.idSigno),
          'fecha': new FormControl(new Date(data.fecha)),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmo': new FormControl(data.ritmo)
        });

      });

    }
  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  seleccionarPaciente(e: any) {
    this.pacienteSeleccionado = e.option.value;
  }

  listarPacientes() {
    this.pacienteService.pacienteCambio.subscribe(data => {
      this.pacientes = data;
    });

    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  openDialog(paciente?: Paciente) {
    let pac = paciente != null ? paciente : new Paciente();

    this.dialog.open(PacienteDialogoComponent, {
      width: '450px',
      data: pac
    });
  }


  aceptar() {
    console.log("Ingreso a metodo aceptar signo-edicion");
    this.signo.idSigno = this.form.value['id'];
    this.signo.paciente = this.form.value['paciente'];
    var tzoffset = (this.form.value['fecha']).getTimezoneOffset() * 60000;
    var localISOTime = (new Date(Date.now() - tzoffset)).toISOString();
    this.signo.fecha = localISOTime;
    this.signo.temperatura = this.form.value['temperatura'];
    this.signo.pulso = this.form.value['pulso'];
    this.signo.ritmo = this.form.value['ritmo'];
    if (this.signo != null && this.signo.idSigno > 0 ) {
      this.signoService.modificar(this.form.value['id'], this.signo).subscribe(() => {
        this.signoService.listar().subscribe(signo => {
          this.signoService.signoCambio.next(signo);
          this.signoService.mensajeCambio.next("Se modificó");
        });
      });
    } else {
      this.signoService.registrar(this.signo).subscribe(() => {
        this.signoService.listar().subscribe(signo => {
          this.signoService.signoCambio.next(signo);
          this.signoService.mensajeCambio.next("Se registró");
        });
      });
    }
    this.router.navigate(['signos']);
  }


}

import { Component, OnInit } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {JwtHelperService} from "@auth0/angular-jwt";

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  usuario: string;
  roles : string[];

  constructor() { }

  ngOnInit() {
    this.obtenerPerfil();
  }
 obtenerPerfil(){
  // console.log("METODO obtenerPerfil");
   const helper = new JwtHelperService();
   let token = sessionStorage.getItem(environment.TOKEN_NAME);
   let decodedToken = helper.decodeToken(token);
   this.roles= decodedToken.authorities;
   this. usuario= decodedToken.user_name;
    // console.log(decodedToken);

 }
}

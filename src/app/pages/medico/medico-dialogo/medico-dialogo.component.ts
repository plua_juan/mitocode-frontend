import {Component, Inject, OnInit} from '@angular/core';
import {Medico} from "../../../_model/medico";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MedicoService} from "../../../_service/medico.service";
import {switchMap, switchMapTo} from "rxjs/operators";

@Component({
  selector: 'app-medico-dialogo',
  templateUrl: './medico-dialogo.component.html',
  styleUrls: ['./medico-dialogo.component.css']
})
export class MedicoDialogoComponent implements OnInit {
  medico: Medico;
  constructor(private dialogRef: MatDialogRef<MedicoDialogoComponent>, @Inject(MAT_DIALOG_DATA) private data: Medico, private medicoService: MedicoService) { }


  ngOnInit() {

    this.medico = new Medico();
    this.medico.idMedico = this.data.idMedico;
    this.medico.nombres = this.data.nombres;
    this.medico.apellidos = this.data.apellidos;
    this.medico.cmp = this.data.cmp;
  }
  cancelar() {
    this.dialogRef.close();
  }
  operar() {
    if (this.medico != null && this.medico.idMedico > 0) {
      //BUENA PRACTICA
      this.medicoService.modificar(this.data.idMedico,this.medico).pipe(switchMap(() => {
        return this.medicoService.listar();
      })).subscribe(medicos => {
        this.medicoService.medicoCambio.next(medicos);
        this.medicoService.mensajeCambio.next("SE MODIFICO");
      });
    } else {
      this.medicoService.registrar(this.medico).pipe(switchMap(() => {
        return this.medicoService.listar();
      })).subscribe(medicos =>{
        this.medicoService.medicoCambio.next(medicos);
        this.medicoService.mensajeCambio.next("SE REGISTRO");
      })
    }
    this.dialogRef.close();
  }
}

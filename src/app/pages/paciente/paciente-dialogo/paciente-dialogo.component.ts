import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Paciente} from "../../../_model/paciente";
import {PacienteService} from "../../../_service/paciente.service";
import {switchMap} from "rxjs/operators";
import {FormControl, FormGroup} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-paciente-dialogo',
  templateUrl: './paciente-dialogo.component.html',
  styleUrls: ['./paciente-dialogo.component.css']
})
export class PacienteDialogoComponent implements OnInit {
  form: FormGroup;
  paciente: Paciente;
  constructor(private dialogRef: MatDialogRef<PacienteDialogoComponent>, @Inject(MAT_DIALOG_DATA) private data: Paciente, private pacienteService: PacienteService,private snackBar: MatSnackBar) { }


  ngOnInit() {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'dni': new FormControl(''),
      'direccion': new FormControl(''),
      'telefono': new FormControl('')
    });
  }
  cancelar() {
    this.dialogRef.close();
  }
  operar() {
    let paciente = new Paciente();
    paciente.nombres = this.form.value['nombres'];
    paciente.apellidos = this.form.value['apellidos'];
    paciente.dni = this.form.value['dni'];
    paciente.direccion = this.form.value['direccion'];
    paciente.telefono = this.form.value['telefono'];
//console.log(paciente);
    //servicio de registro
   this.pacienteService.registrar(paciente).subscribe(() => {
      this.pacienteService.listar().subscribe(data => {
        this.pacienteService.pacienteCambio.next(data);
        this.snackBar.open("Se registró", "Aviso", { duration: 2000 });
      });
    });
    this.dialogRef.close();

  }



}

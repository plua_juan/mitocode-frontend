import { Injectable } from '@angular/core';
import {Subject} from "rxjs";
import {Signo} from "../_model/signo";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {FiltroConsultaDTO} from "../_dto/filtroConsultaDTO";
import {Consulta} from "../_model/consulta";
import {FiltroSignoDTO} from "../_dto/filtroSignoDTO";

@Injectable({
  providedIn: 'root'
})
export class SignoService {

  signoCambio = new Subject<Signo[]>();
  mensajeCambio = new Subject<string>();
  url: string = `${environment.HOST}/signosVitales`;

  constructor(private  http: HttpClient) {
  }

  listar() {
    return this.http.get<Signo[]>(this.url);
  }

  listarPorId(idSigno: number) {
    return this.http.get<Signo>(`${this.url}/${idSigno}`);
  }

  registrar(signo: Signo) {
    return this.http.post(this.url, signo);
  }

  modificar(idSigno: number,signo: Signo) {
    return this.http.put(`${this.url}/${idSigno}`, signo);
  }

  eliminar(idSigno: number) {
    return this.http.delete(`${this.url}/${idSigno}`);
  }

  buscar(filtroSigno: FiltroSignoDTO) {
    return this.http.post<Signo[]>(`${this.url}/buscar`, filtroSigno);
  }
}

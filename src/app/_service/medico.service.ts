import { Injectable } from '@angular/core';
import {Subject} from "rxjs";
import {Medico} from "../_model/medico";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  medicoCambio = new Subject<Medico[]>();
  mensajeCambio = new Subject<string>();
  url: string = `${environment.HOST}/medicos`;

  constructor(private  http: HttpClient) {
  }

  listar() {
    return this.http.get<Medico[]>(this.url);
  }

  listarPorId(idMedico: number) {
    return this.http.get<Medico>(`${this.url}/${idMedico}`);
  }

  registrar(medico: Medico) {
    return this.http.post(this.url, medico);
  }

  modificar(idMedico: number,medico: Medico) {
    return this.http.put(`${this.url}/${idMedico}`, medico);
  }

  eliminar(idMedico: number) {
    return this.http.delete(`${this.url}/${idMedico}`);
  }
}

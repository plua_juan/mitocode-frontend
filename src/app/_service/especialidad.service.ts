import { Injectable } from '@angular/core';
import {Subject} from "rxjs";
import {Especialidad} from "../_model/especialidad";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EspecialidadService {

  especialidadCambio = new Subject<Especialidad[]>();
  mensajeCambio = new Subject<string>();
  url: string = `${environment.HOST}/especialidades`;

  constructor(private  http: HttpClient) {
  }

  listar() {
    return this.http.get<Especialidad[]>(this.url);
  }

  listarPorId(idEspecialidad: number) {
    return this.http.get<Especialidad>(`${this.url}/${idEspecialidad}`);
  }

  registrar(especialidad: Especialidad) {
    return this.http.post(this.url, especialidad);
  }

  modificar(idEspecialidad: number,especialidad: Especialidad) {
    return this.http.put(`${this.url}/${idEspecialidad}`, especialidad);
  }

  eliminar(idEspecialidad: number) {
    return this.http.delete(`${this.url}/${idEspecialidad}`);
  }
}

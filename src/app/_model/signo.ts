import {Paciente} from "./paciente";
export class Signo {
  idSigno: number;
  temperatura: string;
  fecha: string;
  pulso: string;
  ritmo: string;
  paciente: Paciente;
  nombrePaciente: string;
}
